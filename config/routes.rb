Rails.application.routes.draw do
  resources :meetings
  resources :posts

  root 'home#index'
end
